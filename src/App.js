import React from 'react';

import List from "./components/List"

class App extends React.Component {
  render(){
const categories = ["shirts", "hats", "shoes"];
return (<div className="App">
<header>
</header>
<div>
<List categories={categories}/>
</div>
<footer></footer>
</div>)
  }
}

export default App;