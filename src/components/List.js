import React from 'react';

import Items from './Items';

class List extends React.Component { 
  render(){
    return(
  <ul>
    {
		this.props.categories.map((ele, i) => <Items item={ele} />)
		}
  </ul>
)
}
}

export default List;
